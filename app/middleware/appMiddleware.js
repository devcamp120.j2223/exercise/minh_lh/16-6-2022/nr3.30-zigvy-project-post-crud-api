// tạo middleware
const appMiddleware = (request, response, next) => {
    console.log(`Time: ${new Date()} - Url: ${request.url} - Method: ${request.method}`)
    next()
}
//export
module.exports = appMiddleware